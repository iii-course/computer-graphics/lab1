package com.example.lab1;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import javafx.scene.shape.*;


public class MainApplication extends Application {
    private final int windowHeight = 480;
    private final int windowWidth = 640;
    private final int centreX = windowWidth / 2;
    private final int centreY = windowHeight / 2;

    @Override
    public void start(Stage stage) {
        Group root = new Group();
        Scene scene = new Scene(root, windowWidth, windowHeight);

        drawButterfly(root);
        //drawSun(root, scene);

        stage.setTitle("Lab1");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

    private void drawSun(Group root, Scene scene) {
        scene.setFill(Color.ORANGE);

        drawSunBody(root);
        drawSunMouth(root);
        drawSunEyes(root);
    }

    private void drawButterfly(Group root) {
        int bodyRadiusX = 20;
        int bodyRadiusY = 80;

        drawBody(root, bodyRadiusX, bodyRadiusY);
        drawWhiskers(root, bodyRadiusX, bodyRadiusY);
        drawWings(root);
    }

    private void drawBody(Group root, int bodyRadiusX, int bodyRadiusY) {
        Ellipse body = new Ellipse(centreX, centreY, bodyRadiusX, bodyRadiusY);
        body.setFill(Color.LIME);
        root.getChildren().add(body);
    }

    private void drawWhiskers(Group root, int bodyRadiusX, int bodyRadiusY) {
        final int whiskerDepth = 15;
        final int whiskerHeight = 100;
        final int whiskerBase = 10;

        Line whiskerLeft = new Line(
                centreX - bodyRadiusX + whiskerDepth,
                centreY - bodyRadiusY + whiskerDepth,
                centreX - bodyRadiusX + whiskerDepth - whiskerBase,
                centreY - bodyRadiusY + whiskerDepth - whiskerHeight
        );

        Line whiskerRight = new Line(
                centreX + bodyRadiusX - whiskerDepth,
                centreY - bodyRadiusY + whiskerDepth,
                centreX + bodyRadiusX - whiskerDepth + whiskerBase,
                centreY - bodyRadiusY + whiskerDepth - whiskerHeight
        );

        root.getChildren().add(whiskerLeft);
        root.getChildren().add(whiskerRight);
    }

    private void drawWings(Group root) {
        Polygon wingLeftTop = new Polygon(
                centreX, centreY,
                centreX - 100, centreY - 140,
                centreX - 200, centreY - 30
        );
        wingLeftTop.setFill(Color.CYAN);
        root.getChildren().add(wingLeftTop);

        Polygon wingRightTop = new Polygon(
                centreX, centreY,
                centreX + 100, centreY - 140,
                centreX + 200, centreY - 30
        );
        wingRightTop.setFill(Color.CYAN);
        root.getChildren().add(wingRightTop);

        Polygon wingLeftBottom = new Polygon(
                centreX, centreY,
                centreX - 100, centreY + 140,
                centreX - 200, centreY + 30
        );
        wingLeftBottom.setFill(Color.CYAN);
        root.getChildren().add(wingLeftBottom);

        Polygon wingRightBottom = new Polygon(
                centreX, centreY,
                centreX + 100, centreY + 140,
                centreX + 200, centreY + 30
        );
        wingRightBottom.setFill(Color.CYAN);
        root.getChildren().add(wingRightBottom);
    }

    private void drawSunBody(Group root) {
        final int numberOfRays = 10;
        final int rayLength = 180;
        final int angleStep = 360 / numberOfRays;
        final int bodyRadius = 100;

        Polygon body = new Polygon();

        for (int currentAngle = 0; currentAngle < 360; currentAngle += angleStep) {
            double nextPointX = centreX + bodyRadius * Math.cos(Math.toRadians(currentAngle));
            double nextPointY = centreY + bodyRadius * Math.sin(Math.toRadians(currentAngle));
            body.getPoints().add(nextPointX);
            body.getPoints().add(nextPointY);

            Line ray = getSunRay(rayLength);
            ray.getTransforms().add(new Rotate(currentAngle, centreX, centreY));
            root.getChildren().add(ray);
        }

        body.setFill(Color.YELLOW);
        root.getChildren().add(body);
    }

    private void drawSunEyes(Group root) {
        final int eyesWidth = 80;
        final int eyesOffsetFromCentre = 30;

        Circle eyeLeft = new Circle(
                centreX - eyesWidth / 2,
                centreY - eyesOffsetFromCentre,
                5
        );
        eyeLeft.setFill(Color.BLUE);
        root.getChildren().add(eyeLeft);

        Circle eyeRight = new Circle(
                centreX + eyesWidth / 2,
                centreY - eyesOffsetFromCentre,
                5
        );
        eyeRight.setFill(Color.BLUE);
        root.getChildren().add(eyeRight);
    }

    private void drawSunMouth(Group root) {
        final int mouthDepth = 20;
        final int mouthWidth = 80;
        final int mouthOffsetFromCentre = 20;
        Polygon mouth = new Polygon(
                centreX, mouthOffsetFromCentre + centreY + mouthDepth,
                centreX + mouthWidth / 2, mouthOffsetFromCentre + centreY,
                centreX - mouthWidth / 2, mouthOffsetFromCentre + centreY
        );
        mouth.setFill(Color.RED);
        root.getChildren().add(mouth);
    }

    private Line getSunRay(int rayLength) {
        Line ray = new Line(
                centreX,
                centreY,
                centreX,
                centreY + rayLength
        );
        ray.setStroke(Color.YELLOW);
        ray.setStrokeWidth(5);
        return ray;
    }
}